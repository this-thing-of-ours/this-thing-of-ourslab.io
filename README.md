# This Thing of Ours - Guild Website

News feed, recruitment info, and propaganda videos from This Thing of Ours, a World of Warcraft raiding guild on US - Stormrage.

## Build Info

Will include concrete steps for local building soon (TM), and steps for just posting news.

Stay tuned.


## Support

Based off of the Clean Blog template from [Start Bootstrap](http://startbootstrap.com/template-overviews/clean-blog/). Powered by [Jekyll](http://jekyllrb.com) and hosted on [GitLab](http://www.gitlab.com). Created by [Emacsen](http://us.battle.net/wow/en/character/stormrage/Emacsen/advanced/) [Paul Lambert](https://gitlab.com/lambotpm).
